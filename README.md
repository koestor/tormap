# Tormap
This program fetches the current list of Tor nodes then translates the IP of every node to a location and presents them on a map.

## Installation

To install and generate the html files run the following commands. You may need to install some packages for the `pip` command to finish successfully.

```
git submodule update --recursive --init
python -m venv ./venv
source ./venv
pip install -r requirements.txt
./tormap.py
```

Now all neccecary files are in the folder `html`. To test the server locally you can use the python http module

```
cd html
python -m http.server
```

In your browser navigate to `http://localhost:8000`

## Known Issues
You may get rate limited by some of the geoip providers. Either purchase a key or wait for the limit to get reset.




